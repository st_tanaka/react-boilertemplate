webpack = require 'webpack'
ExtractTextPlugin = require 'extract-text-webpack-plugin'
path = require 'path'

module.exports = [
  {
    entry:
      index: './src/index.cjsx'
    output:
        path: path.join __dirname, '/dist'
        filename: "[name].js"
    resolve:
      extensions: ['.js', '.jsx', '.coffee', '.cjsx']
      modules: [
        path.join __dirname, 'src'
        'node_modules'
      ]
    module:
        rules: [
          {
            test: /\.coffee$/,
            use: [
              {
                loader: 'babel-loader'
                options:
                  presets: ['es2015', 'stage-3']
                  plugins: ['transform-runtime']
              }
              'coffeescript-loader'
            ]
          }
          {
            test: /\.cjsx$/,
            use: [
              {
                loader: 'babel-loader'
                options:
                  presets: ['es2015', 'stage-3', 'react']
                  plugins: ['transform-runtime']
              }
              'coffeescript-loader'
              'cjsx-loader'
            ]
          }
        ]
    plugins: [
      new webpack.DefinePlugin
        'process.env.NODE_ENV': JSON.stringify 'production'
      new webpack.optimize.UglifyJsPlugin
        minimize: true
        compress:
          warnings: false
    ]
  }
  {
    entry:
      style: './src/style/index.styl'
    output:
      path: path.join __dirname, '/dist'
      filename: "[name].css"
    resolve:
      extensions: ['.js', '.css', '.scss', '.styl']
      modules: [
        path.join __dirname, 'src/styles'
        'node_modules'
      ]

    module:
      rules: [
        {
          test: /\.css$/
          use: ExtractTextPlugin.extract
            fallback: 'style-loader'
            use: 'css-loader'
        }
        {
          test: /\.styl$/
          use: ExtractTextPlugin.extract
            fallback: 'style-loader'
            use: ['css-loader', 'stylus-loader']
        }
      ]
    plugins: [
      new webpack.DefinePlugin
        'process.env':
          NODE_ENV: 'production'
      new ExtractTextPlugin '[name].css'
    ]
  }
]
