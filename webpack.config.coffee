webpack = require 'webpack'
ExtractTextPlugin = require 'extract-text-webpack-plugin'
path = require 'path'

module.exports = [
  {
    entry:
      index: './src/index.cjsx'
    output:
      path: path.join __dirname, '/dist'
      filename: "[name].js"
      publicPath: 'http://localhost:8080/'
    resolve:
      extensions: ['.js', '.jsx', '.coffee', '.cjsx']
      modules: [
        path.join __dirname, 'src'
        'node_modules'
      ]
    module:
        rules: [
          {
            test: /\.coffee$/,
            use: [
              {
                loader: 'babel-loader'
                options:
                  presets: ['es2015', 'stage-3']
                  plugins: ['transform-runtime']
              }
              'coffeescript-loader'
            ]
          }
          {
            test: /\.cjsx$/,
            use: [
              {
                loader: 'babel-loader'
                options:
                  presets: ['es2015', 'stage-3', 'react']
                  plugins: ['transform-runtime']
              }
              'coffeescript-loader'
              'cjsx-loader'
            ]
          }
        ]
    devServer:
        contentBase: "./dist"
    devtool: 'source-map'
  }
  {
    entry:
      style: './src/style/index.js'
    output:
      path: path.join __dirname, '/dist'
      filename: "[name].js"
      publicPath: 'http://localhost:8080/'
    resolve:
      extensions: ['.js', '.css', '.scss', '.styl']
      modules: [
        path.join __dirname, 'src/style'
        'node_modules'
      ]
    module:
      rules: [
        {
          test: /\.css$/
          use: [
            'style-loader'
            {
              loader: 'css-loader'
              options:
                sourceMap: true
            }
          ]
        }
        {
          test: /\.styl$/
          use: [
            'style-loader'
            {
              loader: 'css-loader'
              options:
                sourceMap: true
            }
            {
              loader: 'stylus-loader'
              options:
                sourceMap: true
            }
          ]
        }
      ]
    devServer:
        contentBase: "./dist"
    devtool: 'source-map'
  }
]
