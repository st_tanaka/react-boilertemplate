import 'common/polyfill'

import React from 'react'
import {createStore, combineReducers, applyMiddleware, compose} from 'redux'
import createSagaMiddleware from 'redux-saga'
import {Provider} from 'react-redux'
import {render} from 'react-dom'

import root_saga from 'sagas'
import reducers from 'reducers'
import Application from 'app'

composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ or compose

sagaMiddleware = createSagaMiddleware()
reducer = combineReducers reducers
store = createStore reducer, composeEnhancers(
  applyMiddleware sagaMiddleware
)
sagaMiddleware.run root_saga

render(
  <Provider store={store}>
    <Application/>
  </Provider>
  document.getElementById 'app'
)
