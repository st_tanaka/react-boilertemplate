
export ActionTypes = {
  'START_TICK'
  'UPDATE_TICK'
}

export start_tick = -> {type: ActionTypes.START_TICK}
export update_tick = (tick) -> {type: ActionTypes.UPDATE_TICK, payload: {tick}}

export default Actions = {start_tick, update_tick}

