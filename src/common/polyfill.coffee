# polyfill for promise in ie
import 'es6-promise-polyfill'
unless window.Promise?
  window.Promise = Promise
