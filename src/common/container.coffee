import {connect} from 'react-redux'

export default container = (args...) ->
  (component) ->
    c = connect(args...) component
    name = component.name
    c[name] = component if name
    c
