import {delay} from 'redux-saga'
import {call, put, takeEvery} from 'redux-saga/effects'

import Actions, {update_tick} from 'actions'

tick_saga = ->
  tick = 0
  loop
    yield put update_tick ++tick
    yield call delay, 1000

export default root_saga = ->
  yield takeEvery 'START_TICK', tick_saga
