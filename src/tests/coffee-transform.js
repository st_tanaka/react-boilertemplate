coffee = require('coffeescript');
babel = require('babel-jest');

module.exports = {
    process: (src, path) => {
        if (coffee.helpers.isCoffee(path)) {
            return coffee.compile(src, {bare: true});
        }
        if (!/node_modules/.test(path)) {
            return babel.process(src, path);
        }
        return src;
    }
}
