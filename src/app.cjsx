import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import connect from 'common/container'

import Actions from 'actions'

export default Application = ->
  <Hello name="react" />

export Hello = connect(
  ({hello}) -> hello
  (dispatch) -> bindActionCreators Actions, dispatch
) class Hello extends Component
  componentDidMount: ->
    @props.start_tick()
  componentWillUnmount:
    @running = false
  render: ->
    <div>
      <div>Hey, {@props.name}</div>
      <div>{@props.tick}</div>
    </div>

