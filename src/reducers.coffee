import typeToReducer from 'type-to-reducer'

export default reducers =
  hello:
    typeToReducer
      UPDATE_TICK: (state, action) ->
        {tick} = state
        tick =
          if tick >= 30
            1
          else
            tick + 1
        {tick}
      {tick: 0}

