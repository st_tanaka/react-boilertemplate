(function() {
  var ExtractTextPlugin, path, webpack;

  webpack = require('webpack');

  ExtractTextPlugin = require('extract-text-webpack-plugin');

  path = require('path');

  module.exports = [
    {
      output: {
        libraryTarget: 'commonjs2'
      },
      resolve: {
        extensions: ['.js', '.jsx', '.coffee', '.cjsx'],
        modules: [path.join(__dirname, 'src'), 'node_modules']
      },
      module: {
        rules: [
          {
            test: /\.coffee$/,
            use: [
              {
                loader: 'babel-loader',
                options: {
                  presets: ['es2015', 'stage-3'],
                  plugins: [
                    'babel-plugin-webpack-loaders', {
                      config: 'webpack-test.config.coffee'
                    }
                  ]
                }
              }, 'coffeescript-loader'
            ]
          }, {
            test: /\.cjsx$/,
            use: [
              {
                loader: 'babel-loader',
                options: {
                  presets: ['es2015', 'stage-3', 'react'],
                  plugins: ['transform-runtime']
                }
              }, 'coffeescript-loader', 'cjsx-loader'
            ]
          }
        ]
      },
      plugins: [
        new webpack.DefinePlugin({
          'process.env.NODE_ENV': JSON.stringify('production')
        }), new webpack.optimize.UglifyJsPlugin({
          minimize: true,
          compress: {
            warnings: false
          }
        })
      ]
    }
  ];

}).call(this);
