# webpack + React + coffeescript2 + stylus boiler template

typescriptがいい？→typescript, ts-loader
sassで十分？→node-sass, sass-loader

## requirement

- node 7.2+
- npm 4.0+
そこまでいらんかもしれんがまあできるだけ最新。

## usage

```
$ npm install
$ npm run start
```
で、 `http://localhost:8080/`を開く。

ソースファイルを更新すると、自動でブラウザがリロードされる。

プロダクション時は、
```
$npm run production
```


